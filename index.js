const http = require('http');
const createHandler = require('node-gitlab-webhook');
const shell = require('shelljs');
const dotenv = require('dotenv');

// enable dotenv
dotenv.config();

const handler = createHandler([
  { path: '/quantum', secret: process.env.GITLAB_SECRET_KEY },
  { path: '/quantum_dev', secret: process.env.GITLAB_SECRET_KEY },
  { path: '/dashboard', secret: process.env.GITLAB_SECRET_KEY },
  { path: '/dashboard_dev', secret: process.env.GITLAB_SECRET_KEY },
  { path: '/broadcast', secret: process.env.GITLAB_SECRET_KEY },
  { path: '/broadcast_dev', secret: process.env.GITLAB_SECRET_KEY }
]);

http.createServer((req, res) => {
  handler(req, res, (err) => {
    res.statusCode = 404;
    res.end('No such location');
  })
}).listen(parseInt(process.env.SERVER_PORT));

handler.on('error', (err) => {
  console.error('Error: ', err.message);
});

handler.on('push', (event) => {
  console.log(
    'Received push event for %s to %s',
    event.payload.repository.name,
    event.payload.ref
  );
  switch (event.path) {
    case '/quantum':
      shell.exec('./sh/quantum.sh');
      break;
    case '/quantum_dev':
      shell.exec('./sh/quantum_dev.sh');
      break;
    case '/dashboard':
      shell.exec('./sh/dashboard.sh');
    case '/dashboard_dev':
      shell.exec('./sh/dashboard_dev.sh');
    case '/broadcast':
      shell.exec('./sh/broadcast.sh');
    case '/broadcast_dev':
      shell.exec('./sh/broadcast_dev.sh');
    default:
      break;
  }
})
