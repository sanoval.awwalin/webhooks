#!/bin/bash
echo "pulling from upstream"
cd "../dashboard_qis"
git checkout master
git pull

echo "stop docker container"
docker stop dashboard

echo "remove old container"
docker container rm dashboard

echo "rebuild docker image"
docker build --no-cache -t dashboard:latest .

echo "run new container from latest image"
docker run -d --network=quantum_quantum_app -p 3000:3000 --name dashboard  dashboard:latest

echo "remove all unused images"
docker system prune -f

echo "container run successfully"
