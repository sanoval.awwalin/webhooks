#!/bin/bash
echo "pulling from upstream"
cd "../dashboard_qis_dev"
git checkout develop
git pull

echo "stop docker container"
docker stop dashboard_dev

echo "remove old container"
docker container rm dashboard_dev

echo "rebuild docker image"
docker build --no-cache -t dashboard:dev .

echo "run new container from latest image"
docker run -d --network=quantum_quantum_app -p 3001:3000 --name dashboard_dev  dashboard:dev

echo "remove all unused images"
docker system prune -f

echo "container run successfully"
