#!/bin/bash
echo "pulling from upstream"
cd "../quantum_dev"
git checkout develop
git pull

echo "shutting down docker container"
docker-compose -f docker-compose.dev.yml down

echo "recreate docker images"
docker image rm quantum_dev_nodejs:latest
docker-compose -f docker-compose.dev.yml up -d

echo "remove all unused images"
docker system prune -f

echo "quantum container is up"
