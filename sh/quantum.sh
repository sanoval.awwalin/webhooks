#!/bin/bash
echo "pulling from upstream"
cd "../quantum"
git checkout master
git pull

echo "shutting down docker container"
docker-compose down

echo "recreate docker images"
docker image rm quantum_nodejs:latest
docker-compose up -d

echo "remove all unused images"
docker system prune -f

echo "quantum container is up"
