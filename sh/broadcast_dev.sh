#!/bin/bash
echo "pulling from upstream"
cd "../broadcast_dev"
git checkout develop
git pull

echo "stop docker container"
docker stop broadcast_dev

echo "remove old container"
docker container rm broadcast_dev

echo "rebuild docker image"
docker build --no-cache -t broadcast:dev .

echo "run new container from latest image"
docker run -d --network=quantum_quantum_app -p 9801:9801 --name broadcast_dev broadcast:dev

echo "remove all unused images"
docker system prune -f

echo "container run successfully"
