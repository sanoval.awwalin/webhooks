#!/bin/bash
echo "pulling from upstream"
cd "../broadcast"
git checkout master
git pull

echo "stop docker container"
docker stop broadcast

echo "remove old container"
docker container rm broadcast

echo "rebuild docker image"
docker build --no-cache -t broadcast:dev .

echo "run new container from latest image"
docker run -d --network=quantum_quantum_app -p 9800:9800 --name broadcast broadcast:dev

echo "remove all unused images"
docker system prune -f

echo "container run successfully"
